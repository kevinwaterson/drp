<?php

namespace App\Model;

use \Nette\Security\IIdentity;
use \Nette\Security\Passwords;
use \Nette\Security\IAuthenticator;
use \Nette\Database\ResultSet;

class DrpAuthenticator implements IAuthenticator
{
	private $context;

	private $passwords;

	public function __construct(\Nette\Database\Context $context, \Nette\Security\Passwords $passwords)
	{
		$this->context = $context;
		$this->passwords = $passwords;
	}

	public function authenticate(array $credentials): \Nette\Security\IIdentity
	{
		[$username, $password] = $credentials;

		$row = $this->context->table('users')
			->where('username', $username)->fetch();

		if (!$row) {
			throw new \Nette\Security\AuthenticationException('User not found.');
		}

		if (!$this->passwords->verify($password, $row->password)) {
			throw new \Nette\Security\AuthenticationException('Invalid password.');
		}
		
		$roles = [];
		$res = $this->context->query( 'SELECT g.name FROM groups g JOIN user_group ug ON g.id=ug.group_id JOIN users u ON ug.user_id=u.id WHERE u.username=?', $row->username );
		foreach( $res as $r )
		{
			if( !in_array( $r->name, $roles ) )
			{
				$roles[] = $r->name;
			}
		}

		return new \Nette\Security\Identity($row->id, $roles, ['username' => $row->username]);
	}
}
