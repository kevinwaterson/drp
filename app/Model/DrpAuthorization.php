<?php

declare(strict_types=1);

namespace App\Model;

use Nette\Security\IAuthorizator;
use Nette\Security\Permission;

class DrpAuthorizationFactory
{
	protected $db;

	public function __construct(\Nette\Database\Connection $db)
	{
			$this->db = $db;
	}

	public function createAuthorizator(): IAuthorizator
	{
		$sql = "SELECT g.name AS role, r.name AS resource, e.name AS entitlement
			FROM groups g
			JOIN policy p ON g.id=p.group_id
			JOIN resources r ON r.id=p.resource_id
			JOIN entitlements e ON e.id=p.entitlement_id
			JOIN user_group ug ON ug.group_id=g.id
			JOIN users u ON u.id=ug.user_id";
			$roles = $this->db->query($sql);

			$acl = new Permission();
/*
			$acl->addRole( 'guest' );
			$acl->addRole( 'admin' );

			$acl->addResource('backend');
			$acl->addResource('blog');

			$acl->allow('admin', 'backend', 'read');
*/
			foreach($roles as $r)
			{
				if(!in_array($r->role, $acl->getRoles(), true))
				{
					$acl->addRole($r->role);
				}
				if ( !in_array($r->resource, $acl->getResources(), true))
				{
					$acl->addResource($r->resource);
				}
				$acl->allow($r->role, $r->resource, $r->entitlement);
			}

			return $acl;
	}

} // end of class
