<?php

declare(strict_types=1);

namespace App\DefaultModule\Presenters;

use Nette;
use Nette\Application\Helpers;

final class DefaultPresenter extends Nette\Application\UI\Presenter
{
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		### $this->database = $database;
		$path = __DIR__.'/../../layouts/@default.latte';
		// $result = $database->query('SELECT * FROM rows');
		parent::setLayout( $path );
		$this->database = $database;
	}

	public function renderDefault() {
		$pairs = $this->database->fetchPairs('SELECT id, name FROM rows');
		$this->template->rows = $pairs;

	}


}

