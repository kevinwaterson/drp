<?php

namespace App\RecipesModule\Presenters;

// alias Nette for use when extending
use \Nette;
use \Nette\Http\Url;
use \Nette\Utils\Json;
use \Nette\Security\User;
use \Nette\Application\Helpers;
use \Nette\Application\UI\Form;

class AdminPresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	private $db;

	public function __construct(Nette\Database\Context $database)
	{
		$this->db = $database;
		$path = __DIR__.'/../../layouts/@admin.latte';
		$this->setLayout( $path );
	}

	public function renderDefault()
	{
		$this->template->title = "User Admin";
		$this->template->username = $this->getUser()->getIdentity()->username;
		$recipes = $this->db->table('recipe');
		$this->template->recipes = $recipes;
		$this->template->id = $this->getUser()->getId();
	}

	public function renderAdd()
	{
		$this->template->title = "Add New Recipe";
		// array for the measure select field
		$measures = $this->db->table('recipe_measure')->fetchPairs('id', 'name');
		$this->template->measures = $measures;
	}

	protected function createComponentAddRecipeForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('title', 'Title:')
			->setRequired('Please enter a title name.')
			->setHtmlAttribute('placeholder', 'Title of recipe')
			->addRule(Form::MIN_LENGTH, 'Title must be at least %d long', 3);


		$form->addText('description', 'Description:')
			->setRequired('Please enter a description of this recipe.')
			->setHtmlAttribute('placeholder', 'A short description')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);


		// times 
		$hours = [0=>'0 hours', 1=>'1 Hour', 2=>'2 Hours',  3=>'3 Hours', 4=>'4 Hours', 5=>'5 Hours'];
		$mins = [0=>'0 Mins', 5=>'5 Mins', 10=>'10 Mins', 15=>'15 Mins', 20=>'20 Mins', 25=>'25 Mins', 30=>'30 Mins', 35=>'35 Mins', 40=>'40 Mins', 45=>'45 Mins', 50=>'55 Mins'];

		$form->addSelect('prep_time_hours', 'Prep time in hours', [ $hours ])
			->setRequired('Please enter valid Prep Time hours.');
		// $form['prep_time_hours']->setDefaultValue(0);

		$form->addSelect('prep_time_minutes', 'Prep Time in Minutes', [ $mins ])
			->setRequired('Please enter a valid Prep Time Minutes.');

		$form->addSelect('cook_time_hours', 'Cook time in hours', [ $hours ])
			->setRequired('Please enter a valid Cook Time Hours.');
		// $form['time_hours']->setDefaultValue(0);

		$form->addSelect('cook_time_minutes', 'Cook time in Minutes', [ $mins ])
			->setRequired('Please enter a valid Cook Time Minutes.');
		// $form['time_minutes']->setDefaultValue(0);
/*
		$form->addText('time', 'Time to Cook (in mins):')
			->setHtmlType('time')
			->setRequired('Please enter a time of this recipe.')
			->setHtmlAttribute('min', '09:00')
			->setHtmlAttribute('max', '20:00')
			->addRule(Form::MIN_LENGTH, 'time must be at least %d long', 3);
*/


		$cats = $this->db->table('recipe_category')->fetchPairs('id', 'name');
		$form->addSelect('recipe_category_id', 'Recipe Category', [ $cats ]);
		$form['recipe_category_id']->setDefaultValue(2);

		$cus = $this->db->table('recipe_cuisine')->fetchPairs('id', 'name');
		$form->addSelect('recipe_cuisine_id', 'Recipe Cuisine', [ $cus ]);
		$form['recipe_cuisine_id']->setDefaultValue(2);

		$res = $this->db->table('recipe_suitable_for_diet')->fetchPairs('id', 'name');
		$form->addMultiSelect('recipe_suitable_for_diet', 'Recipe Suitable For Diet:', $res)
			->setRequired('Please Select an option for Suitable For Diet.');


		$res = $this->db->table('recipe_suitable_for')->fetchPairs('id', 'name');
		$form->addMultiSelect('recipe_suitable_for', 'Recipe Suitable For:', $res)
			->setRequired('Please Select an option for Suitable For.');

		$res = $this->db->table('recipe_contains')->fetchPairs('id', 'name');
		$form->addMultiSelect('recipe_contains', 'Recipe Contains:', $res)
			->setRequired('Please Select an option for Recipe Contains.');
/*

		// eg: 1
		$form->addText('ingredients_number', 'Ingredients number:')
			->setRequired('Please enter a valid number of ingredients.')
			->setHtmlType('number')
			->setHtmlAttribute('value', '1')
			->setHtmlAttribute('step', 'any')
			->addRule(Form::MIN_LENGTH, 'Number must be at least %d', 1);

		// eg: kg
		$res = $this->db->table('recipe_measure')->fetchPairs('id', 'name');
		$form->addSelect('recipe_measure_id', 'Measure:', $res)
			->setRequired('Please Select an option for Measure.');

		// eg: tomatoes
		$form->addText('ingredients', 'Ingredients:')
			->setRequired('Please enter Ingredients.')
			->setHtmlAttribute('class', 'hint')
			->addRule(Form::MIN_LENGTH, 'Ingredient must be at least %d long', 2);

		$form->addTextArea('method', 'Method:')
			->setHtmlAttribute('rows', '5')
			->addRule(Form::MAX_LENGTH, 'Your Method too long', 10000);
 */
		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addRecipeFormSucceeded'];
		return $form;
	}



	public function addRecipeFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'add')) {
			// throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$prep_minutes = ($values->prep_time_hours*60) + $values->prep_time_minutes;
			$cook_minutes = ($values->cook_time_hours*60) + $values->cook_time_minutes;
			$arr = [];
			$arr['user_id'] = $this->getUser()->getId(); 
			$arr['title'] = $values->title;
			$arr['description'] = $values->description;
			$arr['prep_time'] = $prep_minutes;
			$arr['cook_time'] = $cook_minutes;
			$arr['recipe_cuisine_id'] = $values->recipe_cuisine_id;
			$arr['recipe_category_id'] = $values->recipe_category_id;
			$arr['media_hero_id'] = 1;

			$this->flashMessage("New Recipe Added", 'success');
			// $this->db->beginTransaction();

			// returns last insert id
			$recipe_id = $this->db->table('recipe')->insert( $arr );
			
			// insert recipe_contains from the recipe_contains array
			$contains = $values->recipe_contains;
			foreach( $contains as $recipe_contains_id )
			{
				$arr = ['recipe_id'=>$recipe_id, 'recipe_contains_id'=>$recipe_contains_id];
				// insert the values
				$this->db->table('recipe_contains_link')->insert( $arr );
			}


			// recipe suitable for
			$suitable = $values->recipe_suitable_for;
			foreach( $suitable as $recipe_suitable_for_id )
			{
				$arr = ['recipe_id'=>$recipe_id, 'recipe_suitable_for_id'=>$recipe_suitable_for_id];
				$this->db->table('recipe_suitable_for_link')->insert( $arr );
			}

			// recipe suitable for diet
			$suitable = $values->recipe_suitable_for_diet;
			foreach( $suitable as $recipe_suitable_for_diet_id )
			{
				$arr = ['recipe_id'=>$recipe_id, 'recipe_suitable_for_diet_id'=>$recipe_suitable_for_diet_id];
				$this->db->table('recipe_suitable_for_diet_link')->insert( $arr );
			}

			$i=0;
			$ingredients = $form->getHttpData($form::DATA_TEXT | $form::DATA_KEYS, 'ingredients[]');

			$measures = $form->getHttpData($form::DATA_TEXT, 'recipe_measure_id[]');
			$amounts = $form->getHttpData($form::DATA_TEXT, 'ingredients_number[]');
			$optional = $form->getHttpData($form::DATA_TEXT, 'optional[]');
			foreach( $ingredients as $ing)
			{
				// fetch the ID of the ingredient
				$row = $this->db->fetch('SELECT * FROM recipe_ingredient WHERE name = ?', 'Water');
				$measure_id = $measures[$i];
				$amount = $amounts[$i];
				$opt = isset($optional[$i]); // if the box is set, then good, otherwise, false
				$arr = ['recipe_id'=>$recipe_id, 'recipe_ingredient_id'=>$row->id, 'recipe_measure_id'=>$measure_id, 'ingredient_amount'=>$amount, 'optional'=>$opt];
				$this->db->table('recipe_ingredient_link')->insert( $arr );
				$i++;
			}


			$steps = $form->getHttpData($form::DATA_TEXT | $form::DATA_KEYS, 'steps[]');
			foreach( $steps as $step)
			{
				$arr = ['recipe_id'=>$recipe_id, 'media_id'=>1, 'description'=>$step];
				$this->db->table('recipe_step')->insert( $arr );
			}

			// $this->db->commit();
			$this->redirect('Admin:Default');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Category Failed'.$e->getMessage() );
		}
	}


	public function renderEdit( $id )
	{
		$this->template->title = "Edit Recipe";
		$this->template->edit_id = $id;
		$rec = $this->db->table('recipe')->get($id );
		
		$ings = $this->db->table('recipe_ingredient_link')->where('recipe_id = ?', $id );
		$this->template->ingredients = $ings;

		$recipe_suitable_for = [];
		$res = $this->db->table('recipe_suitable_for_link')->where('recipe_id = ?', $id );
		foreach( $res as $suit )
		{
			array_push($recipe_suitable_for, $suit->recipe_suitable_for_id );
		}


		$recipe_suitable_for_diet = [];
		$res = $this->db->table('recipe_suitable_for_diet_link')->where('recipe_id = ?', $id );
		foreach( $res as $suit )
		{
			array_push($recipe_suitable_for_diet, $suit->recipe_suitable_for_diet_id );
		}

		$recipe_contains = [];
		$contains = $this->db->table('recipe_contains_link')->where('recipe_id = ?', $id );
		foreach( $contains as $con )
		{
			array_push($recipe_contains, $con->recipe_contains_id );
		}

		$form = $this['editRecipeForm'];

		$duration = $rec->prep_time;
		$prep_hours = floor($duration / 60);
		$prep_minutes = ($duration % 60);

		$duration = $rec->cook_time;
		$cook_hours = floor($duration / 60);
		$cook_minutes = ($duration % 60);

		$measures = $this->db->table('recipe_measure')->fetchPairs('id', 'name');
		$this->template->measures = $measures;

		$form->setDefaults([
			'title' => $rec->title,
			'description' => $rec->description,
			'prep_time_hours' => $prep_hours,
			'prep_time_minutes' => $prep_minutes,
			'cook_time_hours' => $cook_hours,
			'cook_time_minutes' => $cook_minutes,
			'recipe_category_id' => $rec->recipe_category_id,
			'recipe_cuisine_id' => $rec->recipe_cuisine_id,
			'recipe_contains' => $recipe_contains,
			'recipe_suitable_for_diet' => $recipe_suitable_for_diet,
			'recipe_suitable_for' => $recipe_suitable_for,
		]);
	}

	protected function createComponentEditRecipeForm( $id )
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('title', 'Tie:')
			->setRequired('Please enter a title name.')
			->setHtmlAttribute('placeholder', 'Title of recipe')
			->addRule(Form::MIN_LENGTH, 'Title must be at least %d long', 3);

		$form->addText('description', 'Description:')
			->setRequired('Please enter a description of this recipe.')
			->setHtmlAttribute('placeholder', 'A short description')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);


		// times 
		$hours = [0=>'0 hours', 1=>'1 Hour', 2=>'2 Hours',  3=>'3 Hours', 4=>'4 Hours', 5=>'5 Hours'];
		$mins = [0=>'0 Mins', 5=>'5 Mins', 10=>'10 Mins', 15=>'15 Mins', 20=>'20 Mins', 25=>'25 Mins', 30=>'30 Mins', 35=>'35 Mins', 40=>'40 Mins', 45=>'45 Mins', 50=>'55 Mins'];


		// prep_duration
		$form->addSelect('prep_time_hours', 'Prep time in hours', [ $hours ])
			->setRequired('Please enter valid Prep Hours.');


		// prep minutes
		$form->addSelect('prep_time_minutes', 'Prep time minutes', [ $mins ])
			->setRequired('Please enter valid Prep Time Minutes.');


		// cook_time_hours
		$form->addSelect('cook_time_hours', 'Cook time in hours', [ $hours ])
			->setRequired('Please enter valid Cook Time Hours.');


		// duration
		$form->addSelect('cook_time_minutes', 'Cook time in hours', [ $mins ])
			->setRequired('Please enter valid Cook Minutes.');

/*
		$form->addText('duration', 'Time to Cook (in mins):')
			->setHtmlType('time')
			->setRequired('Please enter a time of this recipe.')
			->setHtmlAttribute('min', '09:00')
			->setHtmlAttribute('max', '20:00')
			->addRule(Form::MIN_LENGTH, 'time must be at least %d long', 3);
*/


		$cats = $this->db->table('recipe_category')->fetchPairs('id', 'name');
		$form->addSelect('recipe_category_id', 'Recipe Category', [ $cats ]);

		$cus = $this->db->table('recipe_cuisine')->fetchPairs('id', 'name');
		$form->addSelect('recipe_cuisine_id', 'Recipe Cuisine', [ $cus ]);
		$form['recipe_cuisine_id']->setDefaultValue(2);

		$res = $this->db->table('recipe_suitable_for_diet')->fetchPairs('id', 'name');
		$form->addMultiSelect('recipe_suitable_for_diet', 'Recipe Suitable For Diet:', $res)
			->setRequired('Please Select an option for Suitable For Diet.');


		$res = $this->db->table('recipe_suitable_for')->fetchPairs('id', 'name');
		$form->addMultiSelect('recipe_suitable_for', 'Recipe Suitable For:', $res)
			->setRequired('Please Select an option for Suitable For.');

		$res = $this->db->table('recipe_contains')->fetchPairs('id', 'name');
		$form->addMultiSelect('recipe_contains', 'Recipe Contains:', $res)
			->setRequired('Please Select an option for Recipe Contains.');


		// eg: 1
		$form->addText('ingredients_number', 'Ingredients number:')
			->setRequired('Please enter a description of this recipe.')
			->setHtmlType('number')
			->setHtmlAttribute('value', '1')
			->setHtmlAttribute('step', 'any')
			->addRule(Form::MIN_LENGTH, 'Number must be at least %d', 1);

		// eg: kg
		$res = $this->db->table('recipe_measure')->fetchPairs('id', 'name');
		$form->addSelect('recipe_measure_id', 'Measure:', $res)
			->setRequired('Please Select an option for Measure.');

		$form->addSubmit('send', 'Update');

		$form->onSuccess[] = [$this, 'editRecipeFormSucceeded'];
		return $form;
	}



	public function editRecipeFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'add')) {
			// throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$minutes = ($values->duration_hours*60) + $values->duration_minutes;
			$arr = [];
			$arr['user_id'] = $this->getUser()->getId(); 
			$arr['title'] = $values->title;
			$arr['description'] = $values->description;
			$arr['prep_time'] = $minutes;
			$arr['cook_time'] = $minutes;
			$arr['recipe_cuisine_id'] = $values->recipe_cuisine_id;
			$arr['recipe_category_id'] = $values->recipe_category_id;
			$arr['media_hero_id'] = 1;

			$this->flashMessage("New Recipe Added", 'success');
			// $this->db->beginTransaction();

			// returns last insert id
			$recipe_id = $this->db->table('recipe')->insert( $arr );
			
			// insert recipe_contains from the recipe_contains array
			$contains = $values->recipe_contains;
			foreach( $contains as $recipe_contains_id )
			{
				$arr = ['recipe_id'=>$recipe_id, 'recipe_contains_id'=>$recipe_contains_id];
				// insert the values
				$this->db->table('recipe_contains_link')->insert( $arr );
			}


			// recipe suitable for
			$suitable = $values->recipe_suitable_for;
			foreach( $suitable as $recipe_suitable_for_id )
			{
				$arr = ['recipe_id'=>$recipe_id, 'recipe_suitable_for_id'=>$recipe_suitable_for_id];
				$this->db->table('recipe_suitable_for_link')->insert( $arr );
			}

			// recipe suitable for diet
			$suitable = $values->recipe_suitable_for_diet;
			foreach( $suitable as $recipe_suitable_for_diet_id )
			{
				$arr = ['recipe_id'=>$recipe_id, 'recipe_suitable_for_diet_id'=>$recipe_suitable_for_diet_id];
				$this->db->table('recipe_suitable_for_diet_link')->insert( $arr );
			}
			// $this->db->commit();
			$this->redirect('Admin:Default');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Category Failed'.$e->getMessage() );
		}
	}



	protected function startup(): void
	{
	parent::startup();
		if (!$this->getUser()->isAllowed('backend', 'read')) {
			/// throw new Nette\Application\ForbiddenRequestException;
		}
	}


	public function actionDelete( $id )
	{
		if (!$this->getUser()->isAllowed('backend', 'delete')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("Recipe Deleted", 'success');
			$this->db->table('recipe')->where('id', $id)->delete();
			$this->redirect('Admin:Default');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Delete Recipe Failed'.$e->getMessage() );
		}
	}



	/********************** Recipe Suitable for ***********************/

	public function renderSuitableFor()
	{
		$recipe_suitable_for = $this->db->table('recipe_suitable_for');
		$this->template->suitable_for = $recipe_suitable_for;
		$this->template->title = "Add Recipe Suitable For";
	}


	public function renderAddSuitableFor()
	{
		$recipe_suitable_for = $this->db->table('recipe_suitable_for');
		$this->template->recipe_suitable_for = $recipe_suitable_for;
		$this->template->title = "Add Recipe Suitable For";
	}


	protected function createComponentAddRecipeSuitableForForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('name', 'Name:')
			->setRequired('Please enter a name.')
			->addRule(Form::MIN_LENGTH, 'Name must be at least %d long', 3);

		$form->addText('description', 'Description:')
			->setRequired('Please enter a description.')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);

		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addRecipeSuitableForFormSucceeded'];
		return $form;
	}

	public function addRecipeSuitableForFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'create')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("New Recipe Suitable For Added", 'success');
			$this->db->table('recipe_suitable_for')->insert( ['name'=>$values->name, 'description'=>$values->description] );
			$this->redirect('Admin:Suitablefor');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Suitable For Failed'.$e->getMessage() );
		}
	}


	public function actionDeleteSuitableFor( $id )
	{
		if (!$this->getUser()->isAllowed('backend', 'delete')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("Recipe Suitable For Deleted", 'success');
			$this->db->table('recipe_suitable_for')->where('id', $id)->delete();
			$this->redirect('Admin:Suitablefor');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Delete Recipe Suitable For Failed'.$e->getMessage() );
		}
	}


	/*************************************************************************************************/


	/********************** Recipe Cuisine ***********************/

	public function renderCuisine()
	{
		$cuisine = $this->db->table('recipe_cuisine');
		$this->template->cuisine = $cuisine;
		$this->template->title = "Add Recipe Cuisine";
	}


	public function renderAddCuisine()
	{
		$cuisine = $this->db->table('recipe_cuisine');
		$this->template->cuisine = $cuisine;
		$this->template->title = "Add Recipe Cuisine";
	}


	protected function createComponentAddRecipeCuisineForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('name', 'Name:')
			->setRequired('Please enter a name.')
			->addRule(Form::MIN_LENGTH, 'Name must be at least %d long', 3);

		$form->addText('description', 'Description:')
			->setRequired('Please enter a description.')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);

		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addRecipeCuisineFormSucceeded'];
		return $form;
	}

	public function addRecipeCuisineFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'create')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("New Recipe Cuisine Added", 'success');
			$this->db->table('recipe_cuisine')->insert( ['name'=>$values->name, 'description'=>$values->description] );
			$this->redirect('Admin:Cuisine');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Cuisine Failed'.$e->getMessage() );
		}
	}


	public function actionDeleteCuisine( $id )
	{
		if (!$this->getUser()->isAllowed('backend', 'delete')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("Recipe Cuisine Deleted", 'success');
			$this->db->table('recipe_cuisine')->where('id', $id)->delete();
			$this->redirect('Admin:Cuisine');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Delete Recipe Cuisine Failed'.$e->getMessage() );
		}
	}


	/*************************************************************************************************/



	/********************** Recipe Contains **********************/

	public function renderContains()
	{
		$contains = $this->db->table('recipe_contains');
		$this->template->contains = $contains;
		$this->template->title = "Add Recipe Contains";
	}


	public function renderAddContains()
	{
		$contains = $this->db->table('recipe_contains');
		$this->template->contains = $contains;
		$this->template->title = "Add Recipe Contains";
	}


	protected function createComponentAddRecipeContainsForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('name', 'Name:')
			->setRequired('Please enter a name.')
			->addRule(Form::MIN_LENGTH, 'Name must be at least %d long', 3);


		$form->addText('description', 'Description:')
			->setRequired('Please enter a description.')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);

		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addRecipeContainsFormSucceeded'];
		return $form;
	}

	public function addRecipeContainsFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'create')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("New Recipe Category Added", 'success');
			$this->db->table('recipe_contains')->insert( ['name'=>$values->name, 'description'=>$values->description] );
			$this->redirect('Admin:Contains');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Contains Failed'.$e->getMessage() );
		}
	}


	public function actionDeleteContains( $id )
	{
		if (!$this->getUser()->isAllowed('backend', 'delete')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("Recipe Contains Deleted", 'success');
			$this->db->table('recipe_contains')->where('id', $id)->delete();
			$this->redirect('Admin:Contains');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Delete Recipe Contains Failed'.$e->getMessage() );
		}
	}


	/*************************************************************************************************/

	/** Category admin ***/
	public function renderCategory()
	{
		$categories = $this->db->table('recipe_category');
		$this->template->categories = $categories;
	
		$this->template->title = "Add Recipe Category Admin";
		$this->template->username = $this->getUser()->getIdentity()->username;
		$this->template->id = $this->getUser()->getId();
	}

	public function renderAddCategory()
	{
		$this->template->title = "Add Recipe Category";
	}


	protected function createComponentAddRecipeCategoryForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('name', 'Name:')
			->setRequired('Please enter a category name.')
			->addRule(Form::MIN_LENGTH, 'Category name must be at least %d long', 3);


		$form->addText('description', 'Description:')
			->setRequired('Please enter a description of this category.')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);

		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addRecipeCategoryFormSucceeded'];
		return $form;
	}

	public function addRecipeCategoryFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'create')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("New Recipe Category Added", 'success');
			$this->db->table('recipe_category')->insert( ['name'=>$values->name, 'description'=>$values->description] );
			$this->redirect('Admin:Category');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Category Failed'.$e->getMessage() );
		}
	}


	public function actionDeleteCategory( $id )
	{
		if (!$this->getUser()->isAllowed('backend', 'delete')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("Recipe Category Deleted", 'success');
			$this->db->table('recipe_category')->where('id', $id)->delete();
			$this->redirect('Admin:Category');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Delete Recipe Category Failed'.$e->getMessage() );
		}
	}


	/********************** Recipe Ingredients api call ***********************/
	
	public function renderIngredients( $term )
	{
		$ingredients = $this->db->table('recipe_ingredient');
		$this->template->ingredients = $ingredients;
		$this->template->title = "Add Recipe Ingredient";
	}


	public function renderAddIngredient()
	{
		$this->template->title = "Add Recipe Ingredient";
	}


	protected function createComponentAddRecipeIngredientForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('name', 'Name:')
			->setRequired('Please enter a name.')
			->addRule(Form::MIN_LENGTH, 'Name must be at least %d long', 3);

		$form->addText('description', 'Description:')
			->setRequired('Please enter a description.')
			->addRule(Form::MIN_LENGTH, 'Description must be at least %d long', 3);

		$form->addSubmit('send', 'Add');

		$form->onSuccess[] = [$this, 'addRecipeIngredientFormSucceeded'];
		return $form;
	}

	public function addRecipeIngredientFormSucceeded(Form $form, \stdClass $values)
	{
		if (!$this->getUser()->isAllowed('backend', 'create')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("New Recipe Cuisine Added", 'success');
			$this->db->table('recipe_ingredient')->insert( ['name'=>$values->name, 'description'=>$values->description] );
			$this->redirect('Admin:Ingredients');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Add Recipe Ingredient Failed'.$e->getMessage() );
		}
	}


	public function actionDeleteIngredient( $id )
	{
		if (!$this->getUser()->isAllowed('backend', 'delete')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		try {
			$this->flashMessage("Recipe Cuisine Deleted", 'success');
			$this->db->table('recipe_ingredient')->where('id', $id)->delete();
			$this->redirect('Admin:Ingredients');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Delete Recipe Ingredients Failed'.$e->getMessage() );
		}
	}


	/*************************************************************************************************/

	public function actionGetIngredients( $term )
	{
		$data = $this->db->table('recipe_ingredient')->where('name LIKE ?', $term.'%')->fetchPairs('id', 'name');
		$this->sendJson( $data );
	}

} // end of class
