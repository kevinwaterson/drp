<?php

namespace App\RecipesModule\Presenters;

// alias Nette for use when extending
use Nette;
use Nette\Http\Url;
use Nette\Security\User;
use Nette\Application\Helpers;
use Nette\Application\UI\Form;

class DefaultPresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$path = __DIR__.'/../../layouts/@default.latte';
		$this->setLayout( $path );
	}

	public function renderDefault()
	{
		$this->template->title = "Register or Login";
		$this->template->subtitle = 'Registered members have access to added functionality';

		/// $this->template->posts = $this->database->table('lyrics')
		///	->order('id ASC');
			// ->limit(5);
	}


	public function renderShow($id)
	{
		$path = __DIR__.'/../../layouts/@showlyrics.latte';
		$this->setLayout( $path );

		$this->template->title = "Register or Login";
		$this->template->subtitle = 'A collection of Kirtans';

		// get data model and pass to template
		$this->template->post = $this->database->table('lyrics')->get( $id );
		// $this->template->blog = $this->blogRepository->getUser($id);
	}

	protected function createComponentRegisterForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('username', 'Username:')
			->setRequired('Please enter your username.')
			->addRule(Form::MIN_LENGTH, 'Username must be at least %d long', 3);

		$form->addPassword('password', 'Password:')
			->setRequired('Please enter your password.')
			->addRule(Form::MIN_LENGTH, 'Your password has to be at least %d long', 10);

		$form->addPassword('passwordVerify', 'Password again:')
			->setRequired('Fill your password again to check for typo')
			->addRule(Form::EQUAL, 'Password mismatch', $form['password']);

		$form->addText('first_names', 'First Names:')
			->setRequired('Please enter your first names.');

		$form->addText('surname', 'Surname:')
			->setRequired('Please enter your username.')
			->addRule(Form::MIN_LENGTH, 'Username must be at least %d long', 2);

		$form->addSubmit('send', 'Register');

		$form->onSuccess[] = [$this, 'registerFormSucceeded'];
		return $form;
	}

	public function registerFormSucceeded(Form $form, \stdClass $values)
	{
		try {
			// $this->getUser()->login($values->username, $values->password);
			// $post = $this->database->table('users')->insert($values);
			$password = password_hash( $values->password, PASSWORD_BCRYPT, [12] );
			$wanted_values = ['username' => $values->username, 
					'password' => $password,
					'first_names' => $values->first_names,
					'surname' => $values->surname,
					'last_updated' => date('Y-m-d G:i:s')
					];

			$post = $this->database->table('users')->insert($wanted_values);
			$this->flashMessage("Registration Complete", 'success');
			// $this->redirect('show', $post->id);
			$this->redirect('Default:Default');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError( 'Bad vibes '.$e->getMessage() );
		}
	}


	protected function createComponentLoginForm()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'div';
		$renderer->wrappers['pair']['container'] = 'div';
		$renderer->wrappers['label']['container'] = 'div';
		$renderer->wrappers['control']['container'] = 'div';

		$form->addText('username', 'Username:')
			->setRequired('Please enter your username.')
			->addRule(Form::MIN_LENGTH, 'Username must be at least %d long', 3);

		$form->addPassword('password', 'Password:')
			->setRequired('Please enter your password.')
			->addRule(Form::MIN_LENGTH, 'Your password has to be at least %d long', 12);

		$form->addSubmit('send', 'Login');

		$form->onSuccess[] = [$this, 'loginFormSucceeded'];
		return $form;
	}

	public function loginFormSucceeded(Form $form, \stdClass $values)
	{
		try {
			$this->flashMessage("You are now logged in", 'success');
			$user = $this->getUser();
			$user->setExpiration('12 hours');
			$this->getUser()->login($values->username, $values->password);
			$this->redirect('Default:Default');
	
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Login Failed'.$e->getMessage() );
		}
	}

	public function actionlogout()
	{
		$this->flashMessage("You are now logged out", 'success');
		$user = $this->getUser();
		$user->logout();
		$this->redirect('Default:Default');
	}



} // end of class
