CREATE TABLE users (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  username varchar(100) NOT NULL,
	  password varchar(100) NOT NULL,
	  first_names varchar(100) NOT NULL,
	  surname varchar(100) NOT NULL,
	  last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (id)
) ENGINE=InnoDB;
INSERT INTO `users` VALUES (1,'admin','$2y$10$X4M8hAcJkuZkoiaokVS9ROjRFjSpEROzP6icYRyrr.1voFblJMtRC','Hey','Zeus','2020-01-01 08:49:37'),
(2,'kevin','$2y$10$X4M8hAcJkuZkoiaokVS9ROjRFjSpEROzP6icYRyrr.1voFblJMtRC','Kevin','Ford','2020-01-01 08:49:38'),
(3,'sally','$2y$10$X4M8hAcJkuZkoiaokVS9ROjRFjSpEROzP6icYRyrr.1voFblJMtRC','Sally','Brown','2020-01-01 08:49:38'),
(4,'susan','$2y$10$X4M8hAcJkuZkoiaokVS9ROjRFjSpEROzP6icYRyrr.1voFblJMtRC','Susie','Spune','2020-01-01 08:49:38');

CREATE TABLE groups (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  name varchar(100) NOT NULL,
	  description text,
	  PRIMARY KEY (id)
) ENGINE=InnoDB;
INSERT INTO `groups` VALUES (1,'admin','God Mode'),
(2,'moderator','A company admin'),
(3,'members','A logged in user'),
(4,'sales_rep','Sales Representative'),
(5,'guest','Non logged in visitor');

CREATE TABLE entitlements (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  name varchar(100) NOT NULL,
	  description text NOT NULL,
	  PRIMARY KEY (id)
) ENGINE=InnoDB;
INSERT INTO `entitlements` VALUES (1,'create','Able to create or insert'),(2,'read','Read Only'),(3,'update','Able to update'),(4,'delete','Able to delete');

CREATE TABLE resources (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  name varchar(100) NOT NULL,
	  description text NOT NULL,
	  PRIMARY KEY (id)
) ENGINE=InnoDB;
INSERT INTO `resources` VALUES (1,'backend','The Admin backend'),(2,'blog','The Blog');

CREATE TABLE user_group (
	  user_id int(11) NOT NULL,
	  group_id int(11) NOT NULL,
	  KEY user_id (user_id),
	  KEY group_id (group_id),
	  CONSTRAINT user_group_ibfk_1 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
	  CONSTRAINT user_group_ibfk_2 FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE
) ENGINE=InnoDB;
INSERT INTO `user_group` VALUES (1,1),(2,2),(3,4),(4,4);


CREATE TABLE policy (
	  group_id int(11) NOT NULL,
	  entitlement_id int(11) NOT NULL,
	  resource_id int(11) NOT NULL,
	  KEY group_id (group_id),
	  KEY entitlement_id (entitlement_id),
	  KEY resource_id (resource_id),
	  CONSTRAINT policy_ibfk_1 FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE,
	  CONSTRAINT policy_ibfk_2 FOREIGN KEY (entitlement_id) REFERENCES entitlements (id) ON DELETE CASCADE,
	  CONSTRAINT policy_ibfk_3 FOREIGN KEY (resource_id) REFERENCES resources (id) ON DELETE CASCADE
) ENGINE=InnoDB;
INSERT INTO `policy` VALUES (1,1,1),(1,2,1),(1,3,1),(1,4,1),(1,1,2),(1,2,2),(1,3,2),(1,4,2),(5,2,2),(2,1,2),(2,2,2),(2,4,2);

CREATE TABLE recipe_category (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Entree main etc',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_category(id, name, description) VALUES (1, 'entree', 'entree');
INSERT INTO recipe_category(id, name, description) VALUES (2, 'appetizer', 'appetizer');
INSERT INTO recipe_category(id, name, description) VALUES (3, 'main', 'main');
INSERT INTO recipe_category(id, name, description) VALUES (4, 'dessert', 'dessert');
INSERT INTO recipe_category(id, name, description) VALUES (5, 'appertif', 'appertif');

CREATE TABLE recipe_cuisine (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Mexican, Indian',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_cuisine(id, name, description) VALUES (1, 'Indian', 'Indian');
INSERT INTO recipe_cuisine(id, name, description) VALUES (2, 'Mexican', 'Mexican');
INSERT INTO recipe_cuisine(id, name, description) VALUES (3, 'Chinese', 'Chinese');
INSERT INTO recipe_cuisine(id, name, description) VALUES (4, 'Moroccan', 'Moroccan');
INSERT INTO recipe_cuisine(id, name, description) VALUES (5, 'Spanish', 'Spanish');
INSERT INTO recipe_cuisine(id, name, description) VALUES (6, 'English', 'English');
INSERT INTO recipe_cuisine(id, name, description) VALUES (7, 'Australian', 'Australian');
INSERT INTO recipe_cuisine(id, name, description) VALUES (8, 'African', 'African');
INSERT INTO recipe_cuisine(id, name, description) VALUES (9, 'Polish', 'Polish');
INSERT INTO recipe_cuisine(id, name, description) VALUES (10, 'French', 'French');
INSERT INTO recipe_cuisine(id, name, description) VALUES (11, 'Greek', 'Greek');

CREATE TABLE recipe_ingredient (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Flour water...',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_ingredient(id, name, description) VALUES (1, 'Tofu', 'Tofu');
INSERT INTO recipe_ingredient(id, name, description) VALUES (2, 'Plain Flour', 'Greek');
INSERT INTO recipe_ingredient(id, name, description) VALUES (3, 'Wholemeal Flour', 'Greek');
INSERT INTO recipe_ingredient(id, name, description) VALUES (4, 'egg', 'egg');
INSERT INTO recipe_ingredient(id, name, description) VALUES (5, 'milk', 'milk');
INSERT INTO recipe_ingredient(id, name, description) VALUES (6, 'Sugar', 'Sugar');
INSERT INTO recipe_ingredient(id, name, description) VALUES (7, 'Salt', 'Salt');
INSERT INTO recipe_ingredient(id, name, description) VALUES (8, 'Water', 'Water');

CREATE TABLE recipe_measure (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Tablespoon, Cup',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_measure(id, name, description) VALUES (1, 'ml', 'Milliliters');
INSERT INTO recipe_measure(id, name, description) VALUES (2, 'lts', 'Liters');
INSERT INTO recipe_measure(id, name, description) VALUES (3, 'tsp', 'Teaspoon');
INSERT INTO recipe_measure(id, name, description) VALUES (4, 'Tbsp', 'Tablespoon');
INSERT INTO recipe_measure(id, name, description) VALUES (5, 'Cup', 'Cup');
INSERT INTO recipe_measure(id, name, description) VALUES (6, 'gm', 'Grams');
INSERT INTO recipe_measure(id, name, description) VALUES (7, 'kg', 'Kilograms');
INSERT INTO recipe_measure(id, name, description) VALUES (8, 'mg', 'Milligrams');

CREATE TABLE recipe_suitable_for (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Vegans, Celiacs',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_suitable_for(id, name, description) VALUES (1, 'Vegan', 'Vegans');
INSERT INTO recipe_suitable_for(id, name, description) VALUES (2, 'Vegetarian', 'Vegetarianss');
INSERT INTO recipe_suitable_for(id, name, description) VALUES (3, 'Celiac', 'Celiacss');
INSERT INTO recipe_suitable_for(id, name, description) VALUES (4, 'Lactose Intollerant', 'Lactose Intollerants');


CREATE TABLE recipe_suitable_for_diet (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Halal, Kosher',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (1, 'Halal', 'Halal');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (2, 'Diabetic', 'Diabetic');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (3, 'Kosher', 'Kosher');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (4, 'Gluten Free', 'Gluten Free');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (5, 'Paleo', 'Paleo');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (6, 'Low Carb', 'Low Carb');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (7, 'Lactose Free', 'Lactose Free');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (8, 'Salt Free', 'Vegan');
INSERT INTO recipe_suitable_for_diet(id, name, description) VALUES (9, 'Sugar Free', 'Sugar Free');

CREATE TABLE mime_type(
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'mp3, avi',
	extension varchar(100) NOT NULL COMMENT 'video/x-msvideo',
	description TEXT NOT NULL COMMENT 'A short description'
)ENGINE=InnoDB;
INSERT INTO mime_type(id, extension, name, description) VALUES (1, 'aac', 'audio/aac', 'AAC audio');
INSERT INTO mime_type(id, extension, name, description) VALUES (2, 'abw', 'application/x-abiword', 'AbiWord document');
INSERT INTO mime_type(id, extension, name, description) VALUES (3, 'arc', 'application/x-freearc', 'Archive document (multiple files embedded)');
INSERT INTO mime_type(id, extension, name, description) VALUES (4, 'avi', 'video/x-msvideo', 'AVI: Audio Video Interleave');
INSERT INTO mime_type(id, extension, name, description) VALUES (5, 'azw', 'application/vnd.amazon.ebook', 'Amazon Kindle eBook format');
INSERT INTO mime_type(id, extension, name, description) VALUES (6, 'bin', 'application/octet-stream', 'Any kind of binary data');
INSERT INTO mime_type(id, extension, name, description) VALUES (7, 'bmp', 'image/bmp', 'Windows OS/2 Bitmap Graphics');
INSERT INTO mime_type(id, extension, name, description) VALUES (8, 'bz', 'application/x-bzip', 'BZip archive');
INSERT INTO mime_type(id, extension, name, description) VALUES (9, 'bz2', 'application/x-bzip2', 'BZip2 archive');
INSERT INTO mime_type(id, extension, name, description) VALUES (10, 'csh', 'application/x-csh', 'C-Shell script');
INSERT INTO mime_type(id, extension, name, description) VALUES (11, 'css', 'text/css', 'Cascading Style Sheets (CSS)');
INSERT INTO mime_type(id, extension, name, description) VALUES (12, 'csv', 'text/csv', 'Comma-separated values (CSV)');
INSERT INTO mime_type(id, extension, name, description) VALUES (13, 'doc', 'application/msword', 'Microsoft Word');
INSERT INTO mime_type(id, extension, name, description) VALUES (14, 'docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'Microsoft Word (OpenXML)');
INSERT INTO mime_type(id, extension, name, description) VALUES (15, 'eot', 'application/vnd.ms-fontobject', 'MS Embedded OpenType fonts');
INSERT INTO mime_type(id, extension, name, description) VALUES (16, 'epub', 'application/epub+zip', 'Electronic publication (EPUB)');
INSERT INTO mime_type(id, extension, name, description) VALUES (17, 'gz', 'application/gzip', 'GZip Compressed Archive');
INSERT INTO mime_type(id, extension, name, description) VALUES (18, 'gif', 'image/gif', 'Graphics Interchange Format (GIF)');
INSERT INTO mime_type(id, extension, name, description) VALUES (19, 'htm', 'text/html', 'HyperText Markup Language (HTML)');
INSERT INTO mime_type(id, extension, name, description) VALUES (20, 'html', 'text/html', 'HyperText Markup Language (HTML)');
INSERT INTO mime_type(id, extension, name, description) VALUES (21, 'ico', 'image/vnd.microsoft.icon', 'Icon format');
INSERT INTO mime_type(id, extension, name, description) VALUES (22, 'ics', 'text/calendar', 'iCalendar format');
INSERT INTO mime_type(id, extension, name, description) VALUES (23, 'jar', 'application/java-archive', 'Java Archive (JAR)');
INSERT INTO mime_type(id, extension, name, description) VALUES (24, 'jpg', 'image/jpeg', 'JPEG images');
INSERT INTO mime_type(id, extension, name, description) VALUES (25, 'jpeg', 'image/jpeg', 'JPEG images');
INSERT INTO mime_type(id, extension, name, description) VALUES (26, 'js', 'text/javascript', 'JavaScript');
INSERT INTO mime_type(id, extension, name, description) VALUES (27, 'json', 'application/json', 'JSON format');
INSERT INTO mime_type(id, extension, name, description) VALUES (28, 'jsonld', 'application/ld+json', 'JSON-LD format');
INSERT INTO mime_type(id, extension, name, description) VALUES (29, 'mid', 'audio/midi audio/x-midi', 'Musical Instrument Digital Interface (MIDI)');
INSERT INTO mime_type(id, extension, name, description) VALUES (30, 'midi', 'audio/midi audio/x-midi', 'Musical Instrument Digital Interface (MIDI)');
INSERT INTO mime_type(id, extension, name, description) VALUES (31, 'mjs', 'text/javascript', 'JavaScript module');
INSERT INTO mime_type(id, extension, name, description) VALUES (32, 'mp3', 'audio/mpeg', 'MP3 audio');
INSERT INTO mime_type(id, extension, name, description) VALUES (33, 'mpeg', 'video/mpeg', 'MPEG Video');
INSERT INTO mime_type(id, extension, name, description) VALUES (34, 'mpg', 'video/mpeg', 'MPEG Video');
INSERT INTO mime_type(id, extension, name, description) VALUES (35, 'mpkg', 'application/vnd.apple.installer+xml', 'Apple Installer Package');
INSERT INTO mime_type(id, extension, name, description) VALUES (36, 'odp', 'application/vnd.oasis.opendocument.presentation', 'OpenDocument presentation document');
INSERT INTO mime_type(id, extension, name, description) VALUES (37, 'ods', 'application/vnd.oasis.opendocument.spreadsheet', 'OpenDocument spreadsheet document');
INSERT INTO mime_type(id, extension, name, description) VALUES (38, 'odt', 'application/vnd.oasis.opendocument.text', 'OpenDocument text document');
INSERT INTO mime_type(id, extension, name, description) VALUES (39, 'oga', 'audio/ogg', 'OGG audio');
INSERT INTO mime_type(id, extension, name, description) VALUES (40, 'ogv', 'video/ogg', 'OGG video');
INSERT INTO mime_type(id, extension, name, description) VALUES (41, 'ogx', 'application/ogg', 'OGG');
INSERT INTO mime_type(id, extension, name, description) VALUES (42, 'opus', 'audio/opus', 'Opus audio');
INSERT INTO mime_type(id, extension, name, description) VALUES (43, 'otf', 'font/otf', 'OpenType font');
INSERT INTO mime_type(id, extension, name, description) VALUES (44, 'png', 'image/png', 'Portable Network Graphics');
INSERT INTO mime_type(id, extension, name, description) VALUES (45, 'pdf', 'application/pdf', 'Hypertext Preprocessor');
INSERT INTO mime_type(id, extension, name, description) VALUES (46, 'ppt', 'application/vnd.ms-powerpoint', 'Microsoft PowerPoint');
INSERT INTO mime_type(id, extension, name, description) VALUES (47, 'pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'Microsoft PowerPoint (OpenXML)');
INSERT INTO mime_type(id, extension, name, description) VALUES (48, 'rar', 'application/x-rar-compressed', 'RAR archive');
INSERT INTO mime_type(id, extension, name, description) VALUES (49, 'rtf', 'application/rtf', 'Rich Text Format (RTF)');
INSERT INTO mime_type(id, extension, name, description) VALUES (50, 'sh', 'application/x-sh', 'Bourne shell script');
INSERT INTO mime_type(id, extension, name, description) VALUES (51, 'svg', 'image/svg+xml', 'Scalable Vector Graphics (SVG)');
INSERT INTO mime_type(id, extension, name, description) VALUES (52, 'swf', 'application/x-shockwave-flash', 'Adobe Flash document');
INSERT INTO mime_type(id, extension, name, description) VALUES (53, 'tar', 'application/x-tar', 'Tape Archive (TAR)');
INSERT INTO mime_type(id, extension, name, description) VALUES (54, 'tif', 'image/tiff', 'Tagged Image File Format (TIFF)');
INSERT INTO mime_type(id, extension, name, description) VALUES (55, 'tirf', 'image/tiff', 'Tagged Image File Format (TIFF)');
INSERT INTO mime_type(id, extension, name, description) VALUES (56, 'ts', 'video/mp2t', 'MPEG transport stream');
INSERT INTO mime_type(id, extension, name, description) VALUES (57, 'ttf', 'font/ttf', 'TrueType Font');
INSERT INTO mime_type(id, extension, name, description) VALUES (58, 'txt', 'text/plain', 'Text, (generally ASCII or ISO 8859-n)');
INSERT INTO mime_type(id, extension, name, description) VALUES (59, 'vsd', 'application/vnd.visio', 'Microsoft Visio');
INSERT INTO mime_type(id, extension, name, description) VALUES (60, 'wav', 'audio/wav', 'Waveform Audio Format');
INSERT INTO mime_type(id, extension, name, description) VALUES (61, 'weba', 'audio/webm', 'WEBM audio');
INSERT INTO mime_type(id, extension, name, description) VALUES (62, 'webm', 'video/webm', 'WEBM video');
INSERT INTO mime_type(id, extension, name, description) VALUES (63, 'webp', 'image/webp', 'WEBP image');
INSERT INTO mime_type(id, extension, name, description) VALUES (64, 'woff', 'font/woff', 'Web Open Font Format');
INSERT INTO mime_type(id, extension, name, description) VALUES (65, 'woff2', 'font/woff2', 'Web Open Font Format');
INSERT INTO mime_type(id, extension, name, description) VALUES (66, 'xhtml', 'application/xhtml+xml', 'XHTML');
INSERT INTO mime_type(id, extension, name, description) VALUES (67, 'xls', 'application/vnd.ms-excel', 'Microsoft Excel');
INSERT INTO mime_type(id, extension, name, description) VALUES (68, 'xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Microsoft Excel (OpenXML)');
INSERT INTO mime_type(id, extension, name, description) VALUES (69, 'xml', 'application/xml', 'if not readable from casual users (RFC 3023, section 3)');
INSERT INTO mime_type(id, extension, name, description) VALUES (70, 'xml', 'text/xml', 'if readable from casual users (RFC 3023, section 3)');
INSERT INTO mime_type(id, extension, name, description) VALUES (71, 'xul', 'application/vnd.mozilla.xul+xml', 'XUL');
INSERT INTO mime_type(id, extension, name, description) VALUES (72, 'zip', 'application/zip', 'ZIP archive');
INSERT INTO mime_type(id, extension, name, description) VALUES (73, '3gp', 'video/3gpp', '3GPP audio/video container');
INSERT INTO mime_type(id, extension, name, description) VALUES (74, '3gp', 'audio/3gpp', ' if it does not  contain video');
INSERT INTO mime_type(id, extension, name, description) VALUES (75, '3g2', 'video/3gpp2', '3GPP2 audio/video container');
INSERT INTO mime_type(id, extension, name, description) VALUES (76, '3g2', 'audio/3gpp2', 'if it does not contain video');
INSERT INTO mime_type(id, extension, name, description) VALUES (77, '7z', 'application/x-7z-compressed', '7-zip archive');

DROP TABLE IF EXISTS media;
CREATE TABLE media(
id INT PRIMARY KEY AUTO_INCREMENT,
name varchar(100) NOT NULL COMMENT 'Pic of Jam',
mime_type_id INT NOT NULL COMMENT 'jpeg, mp4',
url TEXT NOT NULL COMMENT 'Location of media',
INDEX (mime_type_id),
FOREIGN KEY (mime_type_id) REFERENCES mime_type(id) ON DELETE CASCADE
)ENGINE=InnoDB;
INSERT INTO media(id, name, mime_type_id, url) VALUES (1, 'Plum Jam', 24, '/images/plum_jam.jpg');

CREATE TABLE recipe_contains (
        id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(100) NOT NULL COMMENT 'Nuts, Dairy',
	description TEXT NOT NULL
)ENGINE=InnoDB;
INSERT INTO recipe_contains(id, name, description) VALUES (1, 'Peanuts', 'peanuts');
INSERT INTO recipe_contains(id, name, description) VALUES (2, 'Dairy', 'Dairy');
INSERT INTO recipe_contains(id, name, description) VALUES (3, 'Egg', 'Egg');
INSERT INTO recipe_contains(id, name, description) VALUES (4, 'Fish', 'Fish');
INSERT INTO recipe_contains(id, name, description) VALUES (5, 'Gluten', 'Gluten');
INSERT INTO recipe_contains(id, name, description) VALUES (6, 'Shellfish', 'Shellfish');
INSERT INTO recipe_contains(id, name, description) VALUES (7, 'Soy', 'Soy');
INSERT INTO recipe_contains(id, name, description) VALUES (8, 'Nuts', 'Nuts');
INSERT INTO recipe_contains(id, name, description) VALUES (9, 'Sesame Seeds', 'Sesame Seeds');
INSERT INTO recipe_contains(id, name, description) VALUES (10, 'Wheat', 'Wheat');
INSERT INTO recipe_contains(id, name, description) VALUES (11, 'Lupins', 'Lupins');
INSERT INTO recipe_contains(id, name, description) VALUES (12, 'Sulphites', 'Sulphites');

DROP TABLE IF EXISTS recipe;
CREATE TABLE recipe (
        id INT PRIMARY KEY AUTO_INCREMENT,
	user_id INT NOT NULL,
	title VARCHAR(100) NOT NULL,
	description TEXT NOT NULL,
	prep_time INT NOT NULL COMMENT 'Time in minutes',
	cook_time INT NOT NULL COMMENT 'Time in minutes',
	recipe_cuisine_id INT NOT NULL COMMENT 'entree main..',
	recipe_category_id INT NOT NULL COMMENT 'entree main..',
	media_hero_id INT COMMENT 'Media link for hero image',
        last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	INDEX(user_id),
	INDEX(media_hero_id),
	INDEX(recipe_category_id),
	INDEX(recipe_cuisine_id),
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (media_hero_id) REFERENCES media(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_category_id) REFERENCES recipe_category(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_cuisine_id) REFERENCES recipe_cuisine(id) ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE recipe_contains_link(
	recipe_id INT NOT NULL,
	recipe_contains_id INT NOT NULL,
	INDEX(recipe_id),
	INDEX(recipe_contains_id),
	FOREIGN KEY (recipe_id) REFERENCES recipe(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_contains_id) REFERENCES recipe_contains(id) ON DELETE CASCADE
)ENGINE=InnoDB;


CREATE TABLE recipe_suitable_for_link(
	recipe_id INT NOT NULL,
	recipe_suitable_for_id INT NOT NULL,
	INDEX(recipe_id),
	INDEX(recipe_suitable_for_id),
	FOREIGN KEY (recipe_id) REFERENCES recipe(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_suitable_for_id) REFERENCES recipe_suitable_for(id) ON DELETE CASCADE,
	PRIMARY KEY( recipe_id, recipe_suitable_for_id )
)ENGINE=InnoDB;


CREATE TABLE recipe_suitable_for_diet_link(
	recipe_id INT NOT NULL,
	recipe_suitable_for_diet_id INT NOT NULL,
	INDEX(recipe_id),
	INDEX(recipe_suitable_for_diet_id),
	FOREIGN KEY (recipe_id) REFERENCES recipe(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_suitable_for_diet_id) REFERENCES recipe_suitable_for_diet(id) ON DELETE CASCADE,
	PRIMARY KEY( recipe_id, recipe_suitable_for_diet_id )
)ENGINE=InnoDB;

CREATE TABLE recipe_ingredient_link(
	recipe_id INT NOT NULL,
	recipe_ingredient_id INT NOT NULL,
	recipe_measure_id INT NOT NULL,
	ingredient_amount varchar(20) NOT NULL,
	optional BOOLEAN NOT NULL DEFAULT false,
	INDEX(recipe_id),
	INDEX(recipe_ingredient_id),
	INDEX(recipe_measure_id),
	FOREIGN KEY (recipe_id) REFERENCES recipe(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_ingredient_id) REFERENCES recipe_ingredient(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_measure_id) REFERENCES recipe_measure(id) ON DELETE CASCADE,
	PRIMARY KEY (recipe_id, recipe_ingredient_id, recipe_measure_id)
)ENGINE=InnoDB;


CREATE TABLE recipe_step(
        id INT PRIMARY KEY AUTO_INCREMENT,
	recipe_id INT NOT NULL,
	media_id INT NOT NULL,
	description TEXT NOT NULL,
	INDEX(recipe_id),
	INDEX(media_id),
	FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE,
	FOREIGN KEY (recipe_id) REFERENCES recipe(id) ON DELETE CASCADE
)ENGINE=InnoDB;
