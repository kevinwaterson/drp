<?php

declare(strict_types=1);

namespace App\ErrorModule\Presenters;

use Nette;
use Nette\Application\Helpers;

final class DefaultPresenter extends Nette\Application\UI\Presenter
{
	### public function __construct(Nette\Database\Context $database)
	public function __construct()
	{
		### $this->database = $database;
		$path = __DIR__.'/../../layouts/@default.latte';
		parent::setLayout( $path );
	}

	public function renderDefault() {
		$content = 'this is the default content';
		$this->template->content = $content;
	}
}

