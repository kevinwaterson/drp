<?php

declare(strict_types=1);

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\Helpers;

final class DefaultPresenter extends Nette\Application\UI\Presenter
{
	protected $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$path = __DIR__.'/../../layouts/@admin.latte';
		parent::setLayout( $path );
	}

	public function renderCheatsheet() {
		// $user = $this->getUser();
	}


	public function renderDefault() {
		// $user = $this->getUser();
		$table =  $this->database->table('users');
		$num_users = $table->count('*');
		$this->template->num_users = $num_users;
	}

	protected function startup(): void
	{
		parent::startup();

		if (!$this->getUser()->isAllowed('backend', 'read')) {
			throw new Nette\Application\ForbiddenRequestException;
		}
	}

}

