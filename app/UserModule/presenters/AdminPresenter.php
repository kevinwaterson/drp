<?php

namespace App\UserModule\Presenters;

// alias Nette for use when extending
use \Nette;
use \Nette\Http\Url;
use \Nette\Security\User;
use \Nette\Application\Helpers;
use \Nette\Application\UI\Form;

class AdminPresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
		$path = __DIR__.'/../../layouts/@default.latte';
		$this->setLayout( $path );
	}

	public function renderDefault()
	{
		$this->template->title = "User Admin";
		$this->template->username = $this->getUser()->getIdentity()->username;
		$this->template->id = $this->getUser()->getId();
	}

	protected function startup(): void
	{
		parent::startup();
		if (!$this->getUser()->isAllowed('backend', 'read')) {
			throw new Nette\Application\ForbiddenRequestException;
		}
	}

} // end of class
