This implementation uses the Nette framework (https://nette.org), and is adapted to use a modular system.  
Each module is contained within its own directory, along with any dependencies to enable the successful use of the module.

## install saas (requires ruby)
sudo gem install sass

## To compile foundation...
wget https://github.com/foundation/foundation-sites/archive/v6.5.3.tar.gz

## To compile the CSS with sass
cd www/assets/foundation-sites-6.5.3/scss/  
scss --watch app.scss:../../css/app.css

