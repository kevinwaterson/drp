$(document).foundation();

$('[data-app-dashboard-toggle-shrink]').on('click', function(e) {
  e.preventDefault();
  $(this).parents('.app-dashboard').toggleClass('shrink-medium').toggleClass('shrink-large');
});

// more click
$('.card-profile-stats-more-link').click(function(e){
  e.preventDefault();
  if ( $(".card-profile-stats-more-content").is(':hidden') ) {
    $('.card-profile-stats-more-link').find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
  } else {
    $('.card-profile-stats-more-link').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
  }
  $(this).next('.card-profile-stats-more-content').slideToggle();
});


$(function() {
    $('.confirm').click(function(e) {
	e.preventDefault();
	if(confirm("Are you sure you want to Delete this item?")) {
		location.href = this.href;
	}
    });
});


$(document).ready(function() {

var options = {
	select: function (event, ui) {
		        $("#someid").val(ui.item.value); // display the selected text
		        // $("#someid").val(ui.item.label); // display the selected text
		        // $("#someid").attr('item_id',ui.item.id); // save selected id to hidden input
		    },
	source: "/recipes/admin/getingredients",
	minLength: 2
};
var selector = 'input.hint';
$(document).on('keydown.autocomplete', selector, function() {
	$(this).autocomplete(options);
});


	var max_fields = 30;
	var wrapper = $(".container1");
	var add_button = $(".add_form_field");

	var x = 1;
	$(add_button).click(function(e) {
		e.preventDefault();
		if (x < max_fields) {
			x++;
			// append the ingredientRow, and remove values
			$('#ingredientRow').clone().appendTo(wrapper).find("input").val("").end();
		} else {
			alert('Maximum number of fields reached')
		}
	});

	$(wrapper).on("click", ".deleteRow", function(e) {
		e.preventDefault();
		$(this).parent('div').remove();
		x--;
	})


	// add a step
	// addStepButton
	
	var max_steps = 30;
	var stepContainer = $("#stepContainer");
	var addstepButton = $("#addStepButton");

	var x = 1;
	$(addStepButton).click(function(e) {
		e.preventDefault();
		if (x < max_fields) {
			x++;
			// append the ingredientRow, and remove values
			$('#stepRow').clone().appendTo(stepContainer).find("textarea").val("").end();
		} else {
			alert('Maximum number of fields reached')
		}
	});

	$(wrapper).on("click", ".deleteRow", function(e) {
		e.preventDefault();
		$(this).parent('div').remove();
		x--;
	})



});
